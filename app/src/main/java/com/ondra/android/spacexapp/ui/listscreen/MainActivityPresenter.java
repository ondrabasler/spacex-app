package com.ondra.android.spacexapp.ui.listscreen;

import android.support.annotation.Nullable;


import com.ondra.android.spacexapp.R;
import com.ondra.android.spacexapp.data.models.Launch;
import com.ondra.android.spacexapp.data.source.LaunchDataSource;
import com.ondra.android.spacexapp.data.source.LaunchRepository;

import java.util.List;

import javax.inject.Inject;

final class MainActivityPresenter implements MainActivityContract.Presenter {

    @Nullable
    private MainActivityContract.View mMainView;

    private final LaunchRepository mLaunchRepository;

    @Inject
    MainActivityPresenter(LaunchRepository launchRepository) {
        mLaunchRepository = launchRepository;
    }

        @Override
    public void takeView(MainActivityContract.View view) {
        mMainView = view;
        mMainView.showProgress(true);
        setupList();
    }

    @Override
    public void dropView() {
        mMainView = null;
    }

    private void setupList() {
        mLaunchRepository.getLaunches(new LaunchDataSource.GetLaunchesCallback() {
            @Override
            public void onLaunchesLoaded(List<Launch> launches) {
                if (mMainView != null)
                {
                    mMainView.showProgress(false);
                    mMainView.setListContent(launches);
                    mMainView.setScrollPosition();
                }

            }

            @Override
            public void onDataNotAvailable() {
                if (mMainView != null)
                    mMainView.showErrorSnackbar(R.string.no_connection_error);
            }
        });
    }

    @Override
    public void listItemClicked(Launch item) {
        if (mMainView != null)
            mMainView.navigateToDetailActivity(item);
    }
}