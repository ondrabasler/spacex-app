package com.ondra.android.spacexapp.ui.listscreen;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ondra.android.spacexapp.BaseFragment;
import com.ondra.android.spacexapp.R;
import com.ondra.android.spacexapp.data.models.Launch;
import com.ondra.android.spacexapp.di.scopes.ActivityScoped;
import com.ondra.android.spacexapp.ui.detailscreen.LaunchDetailActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link LaunchAdapter.OnListFragmentInteractionListener}
 * interface.
 */
@ActivityScoped
public class LaunchFragment extends BaseFragment implements MainActivityContract.View, LaunchAdapter.OnListFragmentInteractionListener{
    public static final String EXTRA_SCROLL_POSITION = "com.ondra.android.spacexapp.ui.listscreen.scroll_position";

    int mLastScrollPosition = 0;

    private LaunchAdapter.OnListFragmentInteractionListener mListener;

    @BindView(R.id.list) RecyclerView mRecyclerView;
    @BindView(R.id.progress) ProgressBar mProgressView;

    @Inject
    MainActivityContract.Presenter mPresenter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    @Inject
    public LaunchFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launch_list, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void setListContent(List<Launch> launchList){
        mRecyclerView.setAdapter(new LaunchAdapter(launchList, this));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null)
            mLastScrollPosition = savedInstanceState.getInt(EXTRA_SCROLL_POSITION,0);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mRecyclerView.getLayoutManager() != null) {
            mLastScrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            outState.putInt(EXTRA_SCROLL_POSITION,mLastScrollPosition);
        }
    }

    @Override
    public void onListFragmentInteraction(Launch item) {
        mPresenter.listItemClicked(item);
    }

    @Override
    public void navigateToDetailActivity(Launch item) {
        Intent i = new Intent(getActivity(), LaunchDetailActivity.class);
        i.putExtra(LaunchDetailActivity.EXTRA_LAUNCH, item);
        startActivity(i);
    }

    @Override
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        mRecyclerView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void showErrorSnackbar(int error_message) {
        if (getView() != null)
            Snackbar.make(getView(),getString(error_message),Snackbar.LENGTH_LONG);
    }

    @Override
    public void setScrollPosition() {
        mRecyclerView.scrollToPosition(mLastScrollPosition);
    }
}
