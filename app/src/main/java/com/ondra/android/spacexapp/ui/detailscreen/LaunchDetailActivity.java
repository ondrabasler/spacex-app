package com.ondra.android.spacexapp.ui.detailscreen;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import com.ondra.android.spacexapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class LaunchDetailActivity extends DaggerAppCompatActivity {
    public static final String EXTRA_LAUNCH = "com.ondra.android.spacexapp.ui.detailscreen.launch";

    @BindView(R.id.toolbar) public Toolbar mToolbar;

    @Inject
    LaunchDetailFragment injectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_detail);
        ButterKnife.bind(this);

        // Set up the toolbar
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        FragmentManager fm = getSupportFragmentManager();
        LaunchDetailFragment fragment = (LaunchDetailFragment) fm.findFragmentById(R.id.fragmentContainer);

        if (fragment == null) {
            fragment = injectedFragment;
            fm.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
        }

    }

}
