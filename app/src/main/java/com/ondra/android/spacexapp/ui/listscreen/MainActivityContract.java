package com.ondra.android.spacexapp.ui.listscreen;

import com.ondra.android.spacexapp.BasePresenter;
import com.ondra.android.spacexapp.BaseView;
import com.ondra.android.spacexapp.data.models.Launch;

import java.util.List;

public interface MainActivityContract {

    interface View extends BaseView<Presenter> {
        void setListContent(List<Launch> launchList);

        void navigateToDetailActivity(Launch item);

        void showProgress(boolean show);

        void showErrorSnackbar(int error_message);

        void setScrollPosition();
    }

    interface Presenter extends BasePresenter<View> {
        void listItemClicked(Launch item);
    }
}