package com.ondra.android.spacexapp.ui.listscreen;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.ondra.android.spacexapp.R;
import com.ondra.android.spacexapp.data.models.Launch;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Launch} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class LaunchAdapter extends RecyclerView.Adapter<LaunchAdapter.ViewHolder> {

    private final List<Launch> mValues;
    private final OnListFragmentInteractionListener mListener;

    LaunchAdapter(List<Launch> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public @NonNull ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTitleView.setText(holder.mItem.getMissionName());
        holder.mNumberView.setText(String.valueOf(position+1));
        holder.mDateView.setText(holder.mView.getContext().getString(R.string.launch_time_format, new SimpleDateFormat("d MMM yyyy HH:mm:ss", Locale.US).format(holder.mItem.getLaunchDateUtc())));
        holder.mRocketView.setText(holder.mView.getContext().getString(R.string.rocket_name_format, holder.mItem.getRocket().getRocketName()));

      /*  holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });*/

        if (holder.mItem.getLaunchSuccess() != null && !holder.mItem.getLaunchSuccess()) {
            holder.mView.setBackgroundColor(holder.mRedColor);
        } else {
            if (holder.mItem.getLaunchSuccess() == null) {
                holder.mView.setBackgroundColor(holder.mGrayColor);
            } else {
                holder.mView.setBackgroundColor(Color.TRANSPARENT);
            }
        }


        if (holder.mItem.getLinks().getMissionPatchSmall() != null) {
            Glide.with(holder.mView)
                    .load(holder.mItem.getLinks().getMissionPatchSmall())
                    .apply(new RequestOptions().placeholder(holder.mMisionPatch))
                    .transition(new DrawableTransitionOptions().crossFade())
                    .into(holder.mPatchView);
        } else {
            Glide.with(holder.mView)
                    .load(R.mipmap.ic_launcher_logo_round)
                    .apply(new RequestOptions().placeholder(holder.mMisionPatch))
                    .transition(new DrawableTransitionOptions().crossFade())
                    .into(holder.mPatchView);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        Launch mItem;

        @BindView(R.id.launch_list_item_number) TextView mNumberView;
        @BindView(R.id.launch_list_item_title) TextView mTitleView;
        @BindView(R.id.launch_list_item_rocket) TextView mRocketView;
        @BindView(R.id.launch_list_item_date) TextView mDateView;
        @BindView(R.id.imageView) ImageView mPatchView;

        @BindColor(R.color.item_not_launched) int mGrayColor;
        @BindColor(R.color.item_unsuccessful_launch) int mRedColor;
        @BindDrawable(R.mipmap.ic_launcher_logo_round) Drawable mMisionPatch;

        @OnClick(R.id.list_item)
        public void onClick() {
            if (null != mListener) {
                mListener.onListFragmentInteraction(mItem);
            }
        }

        private ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this,view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitleView.getText() + "'";
        }
    }

    /**
     * This interface must be implemented by fragments that contain this
     * adapter to allow an interaction in this adapter to be communicated
     * to the fragment and its presenter.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Launch item);
    }
}

