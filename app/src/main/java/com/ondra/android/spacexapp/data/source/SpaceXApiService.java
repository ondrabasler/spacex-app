package com.ondra.android.spacexapp.data.source;

import com.ondra.android.spacexapp.data.models.Launch;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface SpaceXApiService {
    @GET("launches")
    Single<List<Launch>> getLaunches();
}