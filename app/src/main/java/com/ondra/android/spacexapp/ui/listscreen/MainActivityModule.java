package com.ondra.android.spacexapp.ui.listscreen;

import com.ondra.android.spacexapp.di.scopes.ActivityScoped;
import com.ondra.android.spacexapp.di.scopes.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract LaunchFragment launchFragment();

    @ActivityScoped
    @Binds
    abstract MainActivityContract.Presenter mainActivityPresenter(MainActivityPresenter presenter);
}