package com.ondra.android.spacexapp.ui.detailscreen;

import android.support.annotation.NonNull;

import com.ondra.android.spacexapp.data.models.Launch;
import com.ondra.android.spacexapp.di.scopes.ActivityScoped;
import com.ondra.android.spacexapp.di.scopes.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

import static com.ondra.android.spacexapp.ui.detailscreen.LaunchDetailActivity.EXTRA_LAUNCH;

@Module
public abstract class LaunchDetailModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract LaunchDetailFragment puzzleDetailFragment();

    @ActivityScoped
    @Binds
    abstract LaunchDetailContract.Presenter detailPresenter(LaunchDetailPresenter presenter);

    @NonNull
    @Provides
    @ActivityScoped
    static Launch provideLaunch(LaunchDetailActivity activity) {
        return activity.getIntent().getExtras().getParcelable(EXTRA_LAUNCH);
    }
}