package com.ondra.android.spacexapp.ui.detailscreen;

import android.support.annotation.NonNull;

import com.ondra.android.spacexapp.BasePresenter;
import com.ondra.android.spacexapp.BaseView;
import com.ondra.android.spacexapp.data.models.Payload;

import java.util.Date;

public interface LaunchDetailContract {

    interface View extends BaseView<Presenter> {
        void showTitle(@NonNull String title);

        void setDescription(@NonNull String text);

        boolean isAdded();

        void showMissionPatchPicture(String missionPatchSmall);

        void setLaunchInfo(int days, String siteNameLong);

        void setLaunchInfo(Date date, String siteNameLong);

        void showPayloadsSection();

        void addPayloadView(Payload payload);
    }

    interface Presenter extends BasePresenter<View> {
    }
}