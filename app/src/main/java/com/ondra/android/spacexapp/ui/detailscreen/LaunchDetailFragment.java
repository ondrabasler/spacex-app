package com.ondra.android.spacexapp.ui.detailscreen;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ondra.android.spacexapp.BaseFragment;
import com.ondra.android.spacexapp.R;
import com.ondra.android.spacexapp.data.models.Payload;
import com.ondra.android.spacexapp.di.scopes.ActivityScoped;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
@ActivityScoped
public class LaunchDetailFragment extends BaseFragment implements LaunchDetailContract.View {

    TextView mTitle;
    ImageView mMissionPatch;
    @BindView(R.id.desc_title) TextView mDescTitle;
    @BindView(R.id.description) TextView mDescription;
    @BindView(R.id.info) TextView mInfo;
    @BindView(R.id.desc_layout) LinearLayout mDescLayout;
    @BindView(R.id.info_layout) LinearLayout mInfoLayout;
    @BindView(R.id.payload_layout) LinearLayout mPayloadLayout;

    @Inject
    LaunchDetailContract.Presenter mPresenter;

    @Inject
    public LaunchDetailFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launch_detail, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        if (getActivity() instanceof LaunchDetailActivity) {
            mMissionPatch = ((LaunchDetailActivity) getActivity()).mToolbar.findViewById(R.id.imagePatchView);
            mTitle = ((LaunchDetailActivity) getActivity()).mToolbar.findViewById(R.id.textview_title);
        }
        return view;
    }

    @Override
    public void showTitle(@NonNull String title) {
        mTitle.setText(title);
    }

    @Override
    public void setDescription(@NonNull String text) {
        mDescription.setText(text);
        mDescLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMissionPatchPicture(String missionPatchSmall) {
        mMissionPatch.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(missionPatchSmall)
                .into(mMissionPatch);
    }

    @Override
    public void setLaunchInfo(int days, String siteNameLong) {
        mInfo.setText(getString(R.string.info_format,String.valueOf(days),siteNameLong));
        mInfoLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void setLaunchInfo(Date date, String siteNameLong) {
        mInfo.setText(getString(R.string.launch_time_format2, new SimpleDateFormat("d MMM yyyy HH:mm:ss", Locale.US).format(date),siteNameLong));
        mInfoLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPayloadsSection() {
        mPayloadLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void addPayloadView(Payload payload) {
        View cardView = getLayoutInflater().inflate(R.layout.fragment_launch_detail_payload,mPayloadLayout,false);
        TextView customer = cardView.findViewById(R.id.payload_customer);
        TextView typeManufacturer = cardView.findViewById(R.id.payload_type_manufacturer);
        TextView mass = cardView.findViewById(R.id.payload_mass);

        customer.setText(android.text.TextUtils.join(", ", payload.getCustomers()));
        typeManufacturer.setText(getString(R.string.type_manufacturer_format,payload.getPayloadType(),payload.getManufacturer()));
        if (payload.getPayloadMassKg() != null) {
            mass.setVisibility(View.VISIBLE);
            mass.setText(getString(R.string.mass_format, String.valueOf(payload.getPayloadMassKg())));
        }
        mPayloadLayout.addView(cardView);
    }
}
