package com.ondra.android.spacexapp.ui.listscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import com.ondra.android.spacexapp.R;
import com.ondra.android.spacexapp.data.models.Launch;
import com.ondra.android.spacexapp.ui.detailscreen.LaunchDetailActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity {
    @BindView(R.id.toolbar) Toolbar mToolbar;

    @Inject
    LaunchFragment injectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Set up the toolbar
        setSupportActionBar(mToolbar);
//        if (getSupportActionBar() != null)
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentManager fm = getSupportFragmentManager();
        LaunchFragment fragment = (LaunchFragment) fm.findFragmentById(R.id.launch_list_fragmentContainer);

        if (fragment == null) {
            fragment = injectedFragment;
            fm.beginTransaction().add(R.id.launch_list_fragmentContainer, fragment).commit();
        }
    }
}
