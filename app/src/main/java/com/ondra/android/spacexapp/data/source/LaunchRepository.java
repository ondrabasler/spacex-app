package com.ondra.android.spacexapp.data.source;

import android.support.annotation.NonNull;

import com.ondra.android.spacexapp.data.models.Launch;

import java.lang.ref.SoftReference;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class LaunchRepository implements LaunchDataSource{
    private SoftReference<List<Launch>> mLaunchCache;
    final private SpaceXApiService mApiService;

    @Inject
    LaunchRepository(SpaceXApiService service) {
        mApiService = service;
    }

    @Override
    public void getLaunches(@NonNull GetLaunchesCallback callback) {
        if (mLaunchCache == null || mLaunchCache.get() == null) {
            getLaunchesRemotely(callback);
        } else {
            callback.onLaunchesLoaded(mLaunchCache.get());
        }
            
    }

    private void getLaunchesRemotely(final GetLaunchesCallback callback) {
        mApiService.getLaunches().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Launch>>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onSuccess(List<Launch> launches) {
                        refreshCache(launches);
                        callback.onLaunchesLoaded(launches);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        callback.onDataNotAvailable();
                    }
                });

    }

    private void refreshCache(List<Launch> launches) {
        mLaunchCache = new SoftReference<>(launches);
    }
}