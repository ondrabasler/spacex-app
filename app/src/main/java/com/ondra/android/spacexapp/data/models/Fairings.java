
package com.ondra.android.spacexapp.data.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fairings implements Parcelable
{

    @SerializedName("reused")
    @Expose
    private Boolean reused;
    @SerializedName("recovery_attempt")
    @Expose
    private Boolean recoveryAttempt;
    @SerializedName("recovered")
    @Expose
    private Boolean recovered;
    @SerializedName("ship")
    @Expose
    private String ship;
    public final static Parcelable.Creator<Fairings> CREATOR = new Creator<Fairings>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Fairings createFromParcel(Parcel in) {
            return new Fairings(in);
        }

        public Fairings[] newArray(int size) {
            return (new Fairings[size]);
        }

    }
    ;

    protected Fairings(Parcel in) {
        this.reused = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.recoveryAttempt = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.recovered = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.ship = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Fairings() {
    }

    public Boolean getReused() {
        return reused;
    }

    public void setReused(Boolean reused) {
        this.reused = reused;
    }

    public Boolean getRecoveryAttempt() {
        return recoveryAttempt;
    }

    public void setRecoveryAttempt(Boolean recoveryAttempt) {
        this.recoveryAttempt = recoveryAttempt;
    }

    public Boolean getRecovered() {
        return recovered;
    }

    public void setRecovered(Boolean recovered) {
        this.recovered = recovered;
    }

    public String getShip() {
        return ship;
    }

    public void setShip(String ship) {
        this.ship = ship;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(reused);
        dest.writeValue(recoveryAttempt);
        dest.writeValue(recovered);
        dest.writeValue(ship);
    }

    public int describeContents() {
        return  0;
    }

}
