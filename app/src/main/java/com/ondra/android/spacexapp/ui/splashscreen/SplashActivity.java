package com.ondra.android.spacexapp.ui.splashscreen;

import android.content.Intent;
import android.os.Bundle;

import com.ondra.android.spacexapp.ui.listscreen.MainActivity;

import dagger.android.support.DaggerAppCompatActivity;

public class SplashActivity extends DaggerAppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}

