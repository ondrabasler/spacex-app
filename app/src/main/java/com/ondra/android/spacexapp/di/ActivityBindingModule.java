package com.ondra.android.spacexapp.di;

import com.ondra.android.spacexapp.di.scopes.ActivityScoped;
import com.ondra.android.spacexapp.ui.detailscreen.LaunchDetailActivity;
import com.ondra.android.spacexapp.ui.detailscreen.LaunchDetailModule;
import com.ondra.android.spacexapp.ui.listscreen.MainActivity;
import com.ondra.android.spacexapp.ui.listscreen.MainActivityModule;
import com.ondra.android.spacexapp.ui.splashscreen.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * We want Dagger.Android to create a Subcomponent which has a parent Component of whichever module ActivityBindingModule is on,
 * in our case that will be UserComponent. The beautiful part about this setup is that you never need to tell UserComponent that it is going to have all these subcomponents
 * nor do you need to tell these subcomponents that AppComponent exists.
 * We are also telling Dagger.Android that this generated SubComponent needs to include the specified modules and be aware of a scope annotation @ActivityScoped
 * When Dagger.Android annotation processor runs it will create 3 subcomponents for us.
 */
@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = LaunchDetailModule.class)
    abstract LaunchDetailActivity PuzzleDetailActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract SplashActivity splashActivity();
}
