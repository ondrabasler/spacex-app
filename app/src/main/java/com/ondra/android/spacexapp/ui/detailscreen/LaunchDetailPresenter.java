package com.ondra.android.spacexapp.ui.detailscreen;

import android.support.annotation.Nullable;

import com.ondra.android.spacexapp.data.models.Launch;
import com.ondra.android.spacexapp.data.models.Payload;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

final class LaunchDetailPresenter implements LaunchDetailContract.Presenter {

    @Inject
    Launch mLaunch;

    @Nullable
    private LaunchDetailContract.View mLaunchDetailView;


    @Inject
    LaunchDetailPresenter() {
    }

        @Override
    public void takeView(LaunchDetailContract.View view) {
        mLaunchDetailView = view;
        openLaunchDetail();
    }

    @Override
    public void dropView() {
        mLaunchDetailView = null;
    }

    private void openLaunchDetail() {
        if (mLaunchDetailView != null && mLaunchDetailView.isAdded()) {
            if (mLaunch.getUpcoming()) {
                Date startDateValue = Calendar.getInstance().getTime();
                long diff = mLaunch.getLaunchDateUtc().getTime() - startDateValue.getTime();
                int days = Math.round(diff / (1000*60*60*24));
                mLaunchDetailView.setLaunchInfo(days, mLaunch.getLaunchSite().getSiteNameLong());
            } else {
                mLaunchDetailView.setLaunchInfo(mLaunch.getLaunchDateUtc(),mLaunch.getLaunchSite().getSiteNameLong());
            }

            if (mLaunch.getLinks() != null && mLaunch.getLinks().getMissionPatchSmall() != null)
                mLaunchDetailView.showMissionPatchPicture(mLaunch.getLinks().getMissionPatchSmall());

            mLaunchDetailView.showTitle(mLaunch.getMissionName());

            if (mLaunch.getDetails() != null)
                mLaunchDetailView.setDescription(mLaunch.getDetails());

            if (mLaunch.getRocket().getSecondStage().getPayloads().size() > 0)
            {
                List<Payload> payloadList = mLaunch.getRocket().getSecondStage().getPayloads();
                mLaunchDetailView.showPayloadsSection();
                for (Payload payload : payloadList){
                    if (payload.getCustomers() != null && payload.getPayloadType() != null && payload.getManufacturer() != null)
                        mLaunchDetailView.addPayloadView(payload);
                }
            }
        }

    }
}