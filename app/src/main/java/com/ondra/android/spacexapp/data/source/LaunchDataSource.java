package com.ondra.android.spacexapp.data.source;

import android.support.annotation.NonNull;

import com.ondra.android.spacexapp.data.models.Launch;

import java.util.List;

public interface LaunchDataSource {
    interface GetLaunchesCallback {

        void onLaunchesLoaded(List<Launch> launches);

        void onDataNotAvailable();
    }

    void getLaunches(@NonNull GetLaunchesCallback callback);

}
